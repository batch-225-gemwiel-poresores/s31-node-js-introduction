let http = require ("http");

const server = http.createServer((request, response) => {


	if (request.url == '/login') {

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("You are on the login page.")

	} else { 

		response.writeHead(404, {'Content-Type' 
			: 'text/plain'})
		response.end ("I'm sorry the page you're looking for cannot be found.");

	} 

})

.listen(3000)

console.log ('The server is successfully running.');
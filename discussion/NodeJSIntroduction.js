// Node.js Introduction

// Use the "require" directive load Node.js modules
// The "http" module" lets Node.js transfer data using the hyper text transfer protocol (HTTP)

// HTTP is a protocol that allows the fetching of resources such as HTML documents. 

// The message sent by the "client", usually a web browser, called "request"

// The message send by the "server", as an answer are called "responses"

//using this module's createServer() method, we can create an HTTP server that lsiten to request on a specified port and gives repsonses back to client. 

// Sa createServer, dito tayo nag rrequest ng kung ano ang gustong mangyari, at ano ang magging response. 

// A port is a virtual point where network connection start and end. Each port is associated with a specific process or server.

let http = require ("http");

/* http.createServer(function (request, response){


	// use the writeHead() method to:
		// Set a status code for the response - a "200" http status code meands OK
		// set the content-type of the response as plain text message. 
	response.writeHead(200,{'Content-Type' : 'text/plain'});

	// send the response with text content:
	response.end("Welcome to my World");

	// the server will be assigned to port 4000 via the "listen(4000)" method whre the server will listen to any request that are sent to it eventually communicating with our server 
}).listen(4000)


// when server is running, console will print the message: 
console.log('Server is running at localhost:4000')

*/


// ==========================================
// Part 2 with conditionals




const server = http.createServer((request, response) => {

	// Accessing the "greeting" route returns a message of "Hello BPI"

	// request is the object that is sent via the client(browser)

	// the "url" property refers to the url or link in the browser. 

	if (request.url == '/greeting') {

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Welcome to BPI")

	} else if (request.url == '/customer-service') {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Welcome to Customer Service of BPI");
		
	} else { 

		response.writeHead(404, {'Content-type' : 'text/plain'})
		response.end ("Page is not available");

	} 

})

.listen(4000)

console.log ('Server is running on localhost:4000');

























